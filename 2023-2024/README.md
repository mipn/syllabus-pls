# Syllabus PLS

Description des enseignements du parcours programmation et logiciels sûrs.

Chaque étudiant ou étudiante suit les cours suivants :

1. Programmation
    - [Développement JavaScript avancé et ubiquitaire](cours/JSAU.md) (3 ECTS)
    - [Environnements de développements libres](cours/EDL.md) (3 ECTS)
    - [Langages et environnements évolués](cours/LEE.md) (3 ECTS)
    - [Programmation modulaire et résiliente](cours/PMR.md) (3 ECTS)
2. Spécification, sûreté et sécurité
    - [Programmes et preuves](cours/PP.md) (3 ECTS)
    - [Réseaux de Petri de haut niveau](cours/RPHN.md) (3 ECTS)
    - [Spécification des systèmes complexes](cours/SSC.md) (3 ECTS)
    - [Sûreté et sécurité](cours/SurSec.md) (3 ECTS)
3. Enseignements transversaux
    - Anglais (2 ECTS)
    - Innovation (2 ECTS)
    - Soft skills (2 ECTS)
    - Libre (dport, mobilité internationale, activité associative) (Bonus)
4. Deux options parmi :
    - Aide à la décision (3 ECTS)
    - [Analyse des réseaux sociaux](cours/ARS.md) (3 ECTS)
    - Apprentissage de représentations visuelles (3 ECTS)
    - Apprentissage, contraintes, planifications (3 ECTS)
    - [Flux de données et web services](cours/FDWS.md) (3 ECTS)
    - Fouille de données vocales (3 ECTS)
    - [Interaction Homme-Machine](cours/IHM.md) (3 ECTS)
    - [Machine learning et internet des objets](cours/ML-IoT.md) (3 ECTS)
    - [Sûreté des systèmes cyber-physiques](cours/SSCP.md) (3 ECTS)
    - Traitement de données textuelles (3 ECTS)
    - [Design et eXperience utilisateur](cours/DTUX.md) (3 ECTS)

5. un stage de 4 à 6 mois dans un laboratoire de recherche ou en entreprise.

## Où trouver les informations de référence ?
Le master d'informatique est désormais présenté sur le web sur le site de l'institut Galilée, notre composante de rattachement, à l'URL 
https://galilee.univ-paris13.fr/master/master-informatique/ il s'agit de la présentation générale de référence.

Les pages web du directeur du master sont plus complètes https://lipn.univ-paris13.fr/%7Ebennani/Web_Master_Info/Master_Info_2.html

Les modalités d'évaluations des connaissances et compétences sont votées chaque année par la commission de la formation et de la vie universitaire du conseil académique de l'université Sorbonne Paris Nord (Paris 13). Le texte de référence est actuellement un tableau excel qui circule par mail.

Le présent dépôt (https://gitlab.sorbonne-paris-nord.fr/mipn/syllabus-pls) vise à fournir l'information de référence sur les contenus pédagogiques des cours composant le parcours programmation et logiciels sûrs.

Le master 1 ou l'admission extérieure débouchent sur un premier semestre de M2, intitulé semestre 3 car il fait suite aux semestre 1 et 2 de master, avec des enseignements communs au parcours programmation et logiciels sûrs, organisés en deux blocs thématiques le premier autour de la programmation le second autour des spécifications de la sûreté et de la sécurité et un bloc de cours transversaux commun avec d'autres masters. Viennent ensuite des enseignements de semestre 4 qui consistent en des options majoritairement communes avec le parcours exploration informatique des données et décisionnel et le stage. L'obtention du semestre 3 est requis avant le départ en stage.

~~La plateforme à venir ParcoursSup master devrait concerner les titulaires du diplôme national de licence~~ *probablement reporté aux admissions 2023-2024*. Les centre études en France concernent les étranger$\cdot$e$\cdot$s non résident$\cdot$e$\cdot$s en France. La platerforme de l'université eCandidat concerne les autres cas uniquement.

```mermaid
graph TD;
  ParcoursSup-.->M1[master 1];
  CEF-->M1;
  eCandidat-->M1;
  M1-->M2PLS[Admission en M2 PLS];
  eCandidat-->M2PLS;
  CEF-->M2PLS;
  M2PLS-->S3[semestre 3];
  S3-->options[options du semestre 4];
  EID2[M2 EID2]-->options;
  S3-->juryS3[Validation du semestre 3];
  juryS3[Validation du semestre 3]-->stage;
  options-->stage;
  stage-->jury["jury de diplôme"];
```
