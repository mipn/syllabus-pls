~programmation ~"semestre 3"

# Langages et environnements évolués

Cours assuré par plusieurs intervenant$\cdot$e$\cdot$s professionnels, qui partagent leur expérience avec :
- Angular
- Spring
- Hibernate
- la cartographie OpenStreetMap
- les méthodes agiles (SCRUM, SAFe)
- les technologies SalesForce et le métier de consultant

## Objectifs
L'objectif du cours de LEE c'est de permettre aux étudiants de faire face à plusieurs intervenant venant d'entreprises différentes. Ceci leur permettra de découvrir des aspects différents du développement en entreprise ainsi que des technologies utilisées dans des cas d'usages réels. Nous souhaitons que ce cours soit un point d'ancrage pour leurs années futures, ils sauront que certaines bases y ont été posées et qu'ils pourront réutiliser.
Les étudiants peuvent venir de masters différents, cette UE en a conscience et essaie d'être accessible à tout le monde.

## Compétences visées
À la fin de ce cours, les étudiants auront acquis des compétences complémentaires dans différentes technologies utilisées en entreprise. Nous retrouverons par exemple Java et ses Frameworks (Spring / Hibernate ORM), Kotlin, Angular, SalesForce…
Nous visons également des compétences transverses comme l'architecture de projets, les design pattern et la gestion de projet agile.

## Prérequis
Connaissances en Java

## Contenu du cours
Le cours rassemble des cours magistraux sur Java, Kotlin, Spring, Hibernate, Angluar, SalesForce et la méthodologie agile. Il y a également une mise en pratique de Java, Kotlin, Spring et d'Angular.
Pour permettre une meilleure compréhension des technologies, des sujets différents sont pris en exemples dans le but que les étudiants aient du concret à disposition. Les deux exemples sont une gestion de stock et fournitures (Pizzas) et la cartographie avec OpenStreetMap (où un cours dédié y est donné).

## Éléments d'évaluation (MCCC)
L'évaluation se fait sur la mise en application des cours enseignés à travers d'un TP Kotlin et Spring et d'un projet en Angular. Ainsi que sur un devoir sur table regroupant des questions sur tous les enseignements qui ont été donnés.

sup(EvC + Pr, P + Pr) / 2

## Bibliographie
Certains cours sont disponibles en ligne en permanence comme
https://joxit.dev/IG-Master2/ (pas sur qu'il soit à partager dans la plaquette)
Salesforce : un lien personnalisé que je recommande de faire (question et mini challenge pour débuter)
Trailhead est une plateforme d'apprentissage interactive conçue par Salesforce, proposant des modules guidés pour améliorer les compétences des utilisateurs sur cette plateforme
https://trailhead.salesforce.com/fr/users/zameziane/trailmixes/debuter-avec-salesforce
