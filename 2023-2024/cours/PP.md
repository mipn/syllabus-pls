~"spécifications-sûreté-sécurité" ~"semestre 3"

# Preuves et programmes

Ce module est composé de deux parties:

**Partie I**: TODO

**Partie II**: application avec l'assistant d'aide à la preuve **Coq**

Cette partie est composée de 3 cours de 1h30 et de 10h30 de TP sur machines.
Le cours 1 est une introduction à Coq, à sa logique sous-jacente.
Le cours 2 est centré sur des types de données numériques (naturel, entiers relatifs, réels, flottants).
Le cours 3 est un survol de quelques notions: extraction, langage de tactiques, preuve de programmes

Enseignant$\cdot$es : Stefano GUERRINI (partie I) et Micaela MAYERO (partie II)

## Évaluation
(EvC + P) / 2
