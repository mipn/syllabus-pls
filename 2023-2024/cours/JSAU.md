~programmation ~"semestre 3"

# Développement JavaScript avancé et ubiquitaire

Enseignant : Marc-Aurèle DARCHE

## Prérequis

* Connaissances de la programmation en général, niveau moyen
* Connaissances de la programmation en JavaScript, niveau débutant
* Connaissances de l'utilisation de la ligne de commande, niveau moyen
* Connaissances de l'utilisation de Git, niveau moyen
* Disposer d'un ordinateur portable équipé d'un système GNU-Linux ou macOS (environnements virtuels possibles) et d'une connexion réseau fonctionnelle

## Langue
Français


## Objectifs

L'objectif principal de cet UE est de permettre aux étudiants de comprendre et d'être à l'aise avec les différents côtés serveur et client dans la réalisation d'applications basées sur les technologies web, notamment en s'appuyant sur les différentes présences de briques logicielles en JavaScript à tous les niveaux.

Un second objectif est de développer les applications suivant des pratiques agiles d'utilisation de CI-CD et de tests automatisés.


## Compétences visées

* Savoir trouver et exploiter les documentations de référence
* Connaître la syntaxe et les constructions du langage JavaScript moderne
* Savoir utiliser les notions et briques JavaScript les plus importantes (notamment l'asynchronisme)
* Savoir développer des tests automatisés pour les différents types d'application
* Savoir mettre en commun du code pour serveur web, du code pour navigateur web et du code pour application de bureau (Write once, run everywhere!)
* Connaître les problématiques de sécurité spécifiques liées aux différents types d'utilisation et les principes pour diminuer les risques


## Contenu du cours

Cour magistral et TP mêlés.

Pour le TP, réalisation d'un ensemble d'applications, basées sur les technologies web, fournissant un service correspondant à l'état de l'art sur des besoins actuels, avec une base d'une API centrale et différentes frontaux/applications clientes (web, web-app, desktop, mobile).


## Bibliographie

### Documents créés spécifiquement pour l'UE

Le cours et le support de TP sont disponibles en lignes sous licence libre CC-BY-SA 4.0 :

* [Cours](https://aful.gitlab.io/developpement_javascript_avance_ubiquitaire-cours)
* [Support de TP](https://gitlab.com/aful/developpement_javascript_avance_ubiquitaire-tp)

NB : Le contenu du support de TP est en partie occulté pendant toute la durée d'enseignement du module de manière à ce que les élèves ne grillent pas les étapes.

### Autres documents de référence utilisés

* [La référence du langage JavaScript (tous les objets, toutes les fonctions, etc.)](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
* [L'API Web — telle que disponible dans les navigateurs web et dans Node.js](https://developer.mozilla.org/en-US/docs/Web/API)
* [L'API de Node.js — qui fournit des fonctions supplémentaires à Node.js pour faire ce que ne fournit pas l'API Web](https://nodejs.org/api/)
* [L'API d'Express — qui fournit des fonctions supplémentaires pour faciliter le développement de serveur web](https://expressjs.com/en/4x/api.html)
* [REST API Tutorial](https://www.restapitutorial.com/)


## Évaluation

(EvC + P) / 2
