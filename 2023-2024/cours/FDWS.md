# Flux de données et web services

L'utilisation moderne d'Internet nécessite de plus en plus de traitement des données en flux de façon asynchrone, pas seulement pour les données massives. Nous étudierons dans ce cours les flux réactifs et leur implémentation dans Akka en Scala, puis leur mise en œuvre dans des services web (Akka-HTTP, websockets).

Ce cours fait suite au cours programmation modulaire et résiliente mais il peut être suivi après initiation à la bibliothèque asynchrone Akka.

Enseignant : Pierre Boudes

Emploi du temps : une à deux séances par semaine (après-midi), à partir de janvier (mercredi, jeudi, vendredi).
