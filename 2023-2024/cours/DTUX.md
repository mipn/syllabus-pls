~"semestre 4"

# Design et expérience utilisateur

- La **vision stratégique** servant à définir les besoins de l’utilisateur et donc les objectifs,
- les **fonctionnalités** de l’application ou le contenu de l’interface découlant directement des objectifs,
- la **structure**, c’est-à-dire l’enchaînement des pages ou des écrans, selon le design d’interaction et l’architecture d’information, 
- le **squelette** servant à concevoir une interface claire et parfaitement lisible,
- l’**aspect visuel** rendant l’interface plus attractive et désirable.

Enseignante : Anne-Laure THOMAS-DEREPAS
