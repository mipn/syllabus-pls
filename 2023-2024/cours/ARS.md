~"semestre 4"

# Analyse des réseaux sociaux

Introduction aux grands graphes de  terrain : caractéristiques, modèles génératifs et applications. Problématique d’analyse de grands graphes dynamiques : Classements des nœuds, caractérisation et identification des communautés, \u00ad Approches topologiques pour la prévision de liens. Algorithmes de visualisation de grands graphes

Enseignant : Rushed Kanawati

Emploi du temps : mutualisé avec l'école d'ingénieur (janvier-…)
