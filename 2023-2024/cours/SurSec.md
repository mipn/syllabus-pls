~"spécifications-sûreté-sécurité" ~"semestre 3"


# Sûreté et sécurité (G5PRLSSS)
- Introduction à la cybersécurité.
- Tests de pénétration.

Responsables : Guillaume Rousse et Inti Rossenbach

## Prérequis :
Connaissances générales en système, réseau, et développement, notamment :
- connaître HTTP
- connaître SQL
- manipuler un shell Unix

## Objectifs :
Acquisition d'une culture de base en sécurité informatique, portant notamment sur les notions suivantes :
- analyse de risques, menaces
- défense périmétrique, défense en profondeur
- sécurité des accès
- sécurité de la navigation web
- vulnérabilités, exploitation et contre-mesures
- cryptographie
- éthique et réglementation

## Compétences visées :
- Comprendre et savoir expliquer les grandes fonctions sécurité d'un système d'information
- Acquérir les bons réflexes pour mettre en œuvre les mesures de sécurité de base autour d'une application web
- Acquérir des notions permettant de tester la sécurité d'une application dans un réseau
- Savoir chercher des informations fiables dans le domaine de la sécurité

## Contenu du cours :
Description d'attaques réelles
La sécurité et les risques
Équilibre entre la sécurité et les usages
Paysage des menaces
Défense périmétrique (filtrage réseau, des courriels, des accès web)
Défense en profondeur (cloisonnement, antimalwares, correctifs de sécurité, durcissement, VPN)
Cryptographie (symétrique, asymétrique, fonctions de hachage, utilisations combinées, certificats électroniques)
Vulnérabilités (injections, exécutions de commandes, téléversement de fichiers malveillants, CSRF, buffer overflow)
Politique de sécurité
Méthodes de sensibilisation à la sécurité
Sécurité des accès (méthodes d'authentification cliente, mots de passe)
Sécurité de la navigation web
Détection (Traces, détections, contrôles, alertes, scans, tests d'intrusion)
Réaction (Gestion des incidents de sécurité et des crises)
Éthique et légalité
Sécurité des applications (OWASP)
Travaux pratiques

## Évaluation
Sup(ÉvC, P)


## Bibliographie :
Support de cours : https://www.cryptosec.org/docs/CoursCybersecuriteMaster_2023_v7.pdf