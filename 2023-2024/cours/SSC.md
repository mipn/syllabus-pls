~"spécifications-sûreté-sécurité" ~"semestre 3"

# Spécification et résolution de systèmes critiques

## Partie 1 : spécifications formelles

Intervenant 2023-2024 : Étienne André

### Prérequis
* théorie des graphes et automates
* programmation

### Objectifs
* compréhension de la nécessité des spécification et vérification formelles
* initiation aux techniques de méthodes formelles automatisées

### Compétences visées
* spécifier formellement un système concurrent
* spécifier formellement des propriétés de correction dans des logiques temporelles
* comprendre et appliquer des algorithmes de model checking

### Contenu du cours
- automates et spécifications formelles
- logiques temporelles
- algorithmes de model checking
- propriétés d'accessibilité
- model checking symbolique

### Éléments d'évaluation de cette partie
- (Pa + Pr) / 2

### Bibliographie
* Bérard et al.: "Systems and Software Verification, Model-Checking Techniques and Tools", Springer
* Baier and Katoen: "Principles of Model Checking", MIT Press




## Partie 2 : welcome to Burnout corporate!

Intervante 2023-2024 : Marine Megherbi-Torre

### Description générale
Simulation d'un projet en entreprise dans un domaine critique.

- notion de *release audit proof*
- gestion ou résolution d'incident en production

L'idée sera de prendre le rôle du client lors des démo a chaque itération (toutes les deux semaines).
Entre les soucis techniques, les simulations de phishing, les failles de sécurité à résoudre dans un temps imparti ainsi que les impératifs documentaires… la promotion va avoir un concentré de vie dans l'entreprise !

La notation se fera à 50% sur la complétion du projet (mise en prod et fix potentiel avant la deadline)
Et les 50% restants sur la résolution des soucis (ou dans le cas du phishing, le fait de ne pas être tombé dans le piège ou résolution avant la démo)

### Objectifs
immersion dans le monde professionnel, création d'un environnement qui permet aux étudiants de faire des erreurs et d'apprendre à les surmonter, de sortir du mode ‘étudiant' et d'entrer dans celui de ‘salarié/indépendant'

### Compétences visées :
gestion de projet, communication avec des pairs, réaction aux imprévus

### Contenu du cours :
division des étudiants en groupes afin de réaliser différents projets et ou d'intervenir sur différents aspects d'un pôle informatique.

### Éléments d'évaluation de cette partie
ÉvC : présence durant les enseignement, participation, réaction immédiate face à un imprévu
Projet : aboutissement du projet, documentation, viabilité du groupe (interactions entre eux / feedback des étudiants)
Notation : (ÉvC + Pr) / 2


## Éléments d'évaluation globaux
(Pa + Pr1 + ÉvC + Pr2) / 4
