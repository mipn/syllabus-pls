~"semestre 4"

# Interaction homme-machine (G5INFIHM)

Intervenante (2023-2024) : Catherine Recanati

Ce cours ne se restreint pas à l’interaction de l’homme avec un ordinateur. L’Interaction Homme-Machine est la discipline englobant l’ensemble des aspects de la conception, de l’implémentation et de l’évaluation des systèmes informatiques interactifs. Il porte donc aussi sur les interactions de l’homme avec des objets connectés comme une canne pour aveugle, une tronçonneuse ou un fauteuil roulant.

Les premiers cours s’attacheront à définir les contours du domaine et présenteront par ailleurs l’histoire de la micro-informatique et l’historique des interactions Homme-Ordinateur. Mais c’est l’ergonomie qui est au centre du cours.  L’ergonomie implique des disciplines non informatique, comme la psychologie expérimentale.

Pour la validation du module, chaque étudiant devra faire une présentation d’une demie heure - trois quart d’heure sur un sujet donné (choisi parmi une liste, ou sur un autre sujet, pourvu que ce dernier fasse partie du domaine). Cette présentation sera plus proche d’un cours que d’un exposé oral. En particulier, on pourra y inclure des exercices ou des vidéos.

Emploi du temps : vendredi matin, deux séances en novembre. Option commune aux Master EID2 et PLS.

## Évaluation
(2 ÉvC + P) / 3