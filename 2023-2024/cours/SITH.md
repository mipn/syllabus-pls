~"semestre 4"

# Systèmes infinis temporisés et hybrides

Option remplacée par (sûreté des systèmes cyber-physiques)[SSCP.md].

Modélisation et vérification de systèmes infinis, temporisés et hybrides. Automates temporisés, logique temporisée, expression de propriétés temps-réel (TCTL, MTL,…), outil UPPAAL. Automates temporisés/hybrides paramétrés, outil IMITATOR. Systèmes infinis. Modèles stochastiques. Études de cas.

Enseignant : Kaïs Klai

