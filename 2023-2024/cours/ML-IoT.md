~"semestre 4"
# Machine learning et internet des objets

Ce cours donnera une brève introduction à l'internet des objets notamment aux problèmes liés à l'acquisition des données (fonction de transfert, conversion, échantillonnage, etc.) qui montrent que les données subissent des transformations avant d'arriver dans les bases de données utilisées par les algorithmes d'apprentissage. Il mettra en exergue l'impact sur l'apprentissage des problématiques multi-sources, hétérogénéité, décalage temporel, topologies de déploiement, etc. issus
des données de l'IoT.

Enseignants : Aomar Osmani et Massinissa Hamidi.

Emploi du temps : jeudi après-midi + hackathon

