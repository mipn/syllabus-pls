~"spécifications-sûreté-sécurité" ~"semestre 3"

# Réseaux de Petri de haut niveau

Responsable : Kaïs Klaï

## Objectif
- Étudier les réseaux de Petri ordinaires et quelques extensions de ce modèle telles que les réseaux de Petri avec arcs inhibiteurs, réseaux de Petri avec priorité, réseaux de Petri colorés, bien formés, temporisés et temporel.
- Étudier des propriété génériques des réseaux de Petri et être capable d'exprimer des propriété spécifiques
- Pour chaque version des réseaux de Petri, comprendre la syntaxe du modèle et être capable de représenter et analyser sa sémantique sous forme d'un graphe fini ou une abstraction fini de son comportement infini
- Manipuler des outils d'édition et de vérification de systèmes modélisés avec des réseaux de Petri

## Compétences visées
- Maîtriser la syntaxe et la sémantique des réseaux de Petri
- Maîtriser la modélisation et l'analyse des modèles réseaux de Petri.
- Saisir le pouvoir d'expression d'une extension syntaxique d'un modèle et son influence sur le pouvoir d'analyse 
- Être capable de modéliser des systèmes concurrents avec des réseaux de Petri
- Être capable d'analyser les comportements des systèmes et déterminer si une spécification donnée est satisfaite par le système


## Évaluation
Sup(P, (2P + EvC)/3)

