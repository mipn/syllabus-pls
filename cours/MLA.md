~programmation ~"semestre 3"

# Méthodologies et Langages Avancés

Cours assuré par plusieurs intervenant$\cdot$e$\cdot$s professionnels, qui partagent leur expérience avec :
- Spring
- Hibernate
- la cartographie OpenStreetMap
- les documentations techniques
- les méthodes agiles (SCRUM, SAFe)
- les technologies SalesForce et le métier de consultant$\cdot$e

## Objectifs
L'objectif du cours de MLA est de permettre aux étudiant$\cdot$e$\cdot$es de leur permettre de découvrir des aspects différents du développement en entreprise ainsi que des technologies utilisées dans des cas d'usages réels. Dans le cadre d'une immersion dans le monde professionnel, création d'un environnement qui permet aux étudiant$\cdot$es$\cdot$e de faire des erreurs et d'apprendre à les surmonter, de sortir du mode « étudiant » et d'entrer dans celui de « salarié/indépendant ».
Nous souhaitons que ce cours soit un point d'ancrage pour leurs années futures, ils sauront que certaines bases y ont été posées et qu'ils pourront réutiliser.
Les étudiant$\cdot$es$\cdot$e peuvent venir de masters différents ; cette UE en a conscience, et essaie d'être accessible à tout le monde.

## Compétences visées
À la fin de ce cours, les étudiant$\cdot$es$\cdot$e auront acquis des compétences complémentaires dans différentes technologies utilisées en entreprise. Nous retrouverons par exemple Java et ses Frameworks (Spring / Hibernate ORM), Kotlin, SalesForce…
Nous visons également des compétences transverses comme l'architecture de projets, les design pattern et la gestion de projet agile.

## Prérequis
Connaissances en Java

## Contenu du cours
Le cours rassemble des cours magistraux sur Java, Kotlin, Spring, Hibernate, SalesForce et la méthodologie agile. Il y a également une mise en pratique de Java, Kotlin, Spring.
Pour permettre une meilleure compréhension des technologies, des sujets différents sont pris en exemples dans le but que les étudiant$\cdot$es$\cdot$e aient du concret à disposition. Ils simuleront aussi l'intervention sur différents aspects d'un pôle informatique : division des étudiant$\cdot$e$\cdot$es en groupes responsables d'un projet ou d'une partie d'un projet.



## Éléments d'évaluation (MCCC)
* EvC1 : mise en application des cours enseignés à travers d'un TP Kotlin et Spring.
* PA : devoir sur table regroupant des questions sur tous les enseignements techniques qui ont été donnés.
* EvC2 : présence durant les enseignement, participation, réaction immédiate face à un imprévu
* Pr : aboutissement du projet, documentation, viabilité du groupe (interactions entre eux / feedback des étudiant$\cdot$e$\cdot$es)



## Bibliographie

[//]: <> (pas sur que le premier lien soit à partager dans la plaquette)

* Certains cours sont disponibles en ligne en permanence comme
https://joxit.dev/IG-Master2/
* Salesforce : un lien personnalisé que je recommande de faire (question et mini challenge pour débuter)
* Trailhead est une plateforme d'apprentissage interactive conçue par Salesforce, proposant des modules guidés pour améliorer les compétences des utilisateurs sur cette plateforme
https://trailhead.salesforce.com/fr/users/zameziane/trailmixes/debuter-avec-salesforce
