~"semestre 4"

# Mineure parcours P2S : Cartographie et OpenStreetMap
Dans ce module nous apprendrons le fonctionnement des cartes modernes et verrons également comment les utiliser tout en mettant en valeur vos données. Du développement Front End, Back End et de la théorie sont à prévoir.
Ce module peut être vu comme une suite partielle du module de majeure « MLA ».

Enseignant : Jones Magloire

## Prérequis
* Connaissances de la programmation Java, niveau moyen
* Connaissances de la programmation Kotlin, niveau débutant
* Connaissances de la programmation Javascript, niveau débutant
* Connaissances sur git

## Objectifs
* Création d'une application fullstack sur un sujet cartographique
* Gestion d'une base de données géospatiale

## Compétences visées
À la fin de ce cours, les étudiant$\cdot$es$\cdot$e auront acquis des compétences en développement Kotlin, Spring et React ansi qu'une meilleure compréhension de l'univers cartographique et d'OpenStreetMap.

## Éléments d'évaluation
- Pr
