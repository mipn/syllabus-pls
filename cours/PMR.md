~programmation ~"semestre 3"

# Programmation modulaire et résiliente

## Prérequis
Niveau master, fondements de la programmation (lambda-calcul, systèmes de types, polymorphisme), programmation asynchrone et multi-thread et, niveau licence, maîtrise des paradigmes de programmation fonctionnelle et orientée objet, bonne maîtrise de l’environnement GNU / Linux. 


## Objectifs

Maîtriser des paradigmes avancés de programmation adaptés aux infrastructures modernes et à la prédominance du web (programmation fonctionnelle, orienté objet, orienté acteurs, API REST). Utiliser des spécifications (OpenApi3) et un système de types fort et vérifié statiquement pour se prémunir d’un maximum d’erreurs de programmation. Prévoir des stratégies de reprise sur incident.


## Contenu du cours
- Quelques ordres de grandeur à connaître en programmation web
- Programmation fonctionnelle avancée en Scala (pureté, transparence référentielle, monades et leur origine en théorie des catégories)
- Structures de données immutables (Option, List, Vector, Set, Map)
- Typage (polymorphisme, sous-typage et variance, types algébriques, classes et traits)
- Programmation asynchrone (Futures, acteurs avec Pekko/Akka)
- Design d'une API Web de type REST avec OpenApi
- Implémentation en Scala + Pekko d'un backend spécifié avec OpenApi

## Compétences visées
- Programmation en Scala
- Compétences renforcées en programmation fonctionnelle, objet, et asynchrone et orientée acteurs
- Capacité à estimer les performances d'une infrastructure pour aider à décider d'une architecture logicielle
- Capacité à documenter formellement une API REST avec OpenApi et à l'implémenter côté serveur
- Capacité à se prémunir d'erreurs par le typage

## Éléments d'évaluation (MCCC)
trois modalités d'évaluation : évaluation continue (EvC), projet (Pr), partiel de fin de semestre (P) se combinent pour faire deux notes, la moyenne EvC et Pr et la moyenne P et Pr, dont on retient la meilleure comme note à l'UE :
 (EvC + Pr, P + Pr) / 2.

## Ressources


Supports :

- https://mindsized.org/s/prez/cours-pmr-2023.html

Ouvrages :

- Functional programming in Scala,  Michael Pilquist, Rúnar Bjarnason, and Paul Chiusano, Mai 2023, Manning publications
- les documentations officielles de [Scala](https://www.scala-lang.org/) et [Pekko](https://pekko.apache.org/docs/pekko/current)
- voir également la [webographie](https://mindsized.org/s/prez/cours-pmr-2023.html#webographie)

Outils :

- Installation complète avec notamment ammonite le REPL Scala de Li Haoyi [one click install Scala](https://github.com/scala/scala-lang/blob/main/_posts/2020-06-29-one-click-install.md)
- vscode ou Emacs + Metals

Dépôts git et gitlab :

- snippets : https://gitlab.sorbonne-paris-nord.fr/boudes/cours-pmr-2023
- projet anagrammes : https://gitlab.sorbonne-paris-nord.fr/boudes/anagrams_2022
- partie sur Akka/Pekko : https://gitlab.sorbonne-paris-nord.fr/boudes/akkatypedfromthedoc
- Pour le backend du projet (Scala, Pekko, pekko-http, OpenApi3.1) https://gitlab.sorbonne-paris-nord.fr/boudes/tuto-akka-http-openapi
