~"semestre 4"

# Mineure parcours P2S : Maîtrise des crises dans les environnements critiques

## Objectifs :
 Apprendre à maintenir un projet en production.

## Compétences visées :
réactivité, créativité, communication, documentation

## Contenu du cours :
Les étudiants, répartis en différents groupes, devront faire face à des imprévus de production comme une version qui n’est plus supportée et doit être modifiée, un serveur qui tombe en panne, des tentatives de hameçonnage (phishing). Ils apprendront à prendre action dans des délais impartis tout en produisant un résultat adéquat aux besoins live (en production).

Ce module peut être vu comme une suite partielle du module de majeure « MLA ».

## Éléments d’évaluation (MCCC) : EvC

## Intervenante
Marine Megherbi-Torre
