~"semestre 4"

# Mineure parcours P2S : Sûreté des systèmes cyber-physiques

Avec l'essor de l'Internet des objets et des systèmes embarqués, les systèmes cyber-physiques deviennent omniprésents. Ces systèmes combinent des composants de calcul discret (processeur) avec des aspects continus (énergie, température…). Les systèmes cyber-physiques sont très présents notamment dans les transports (aéronautique, aérospatiale, ferroviaire), l'énergie, les systèmes temps-réel ou la santé. Afin de prévenir l'absence de dysfonctionnement à l'exécution, il est essentiel de pouvoir garantir la sûreté et la sécurité de tels systèmes, notamment face à des menaces ou attaques.

Dans ce module, nous abordons des techniques permettant de garantir la sûreté et la sécurité des systèmes cyber-physiques, à l'aide de techniques telles que le model checking quantitatif. Nous effectuons ensuite des applications pratiques à des domaines tels que l'ordonnancement de systèmes temps-réel, le déplacement de robots autonomes ou encore les circuits mémoire.

Mots-clés : systèmes temps-réel, cybersécurité, model checking, vérification formelle

Technologies et logiciels : automates temporisés, Uppaal, IMITATOR

Enseignant: Étienne ANDRÉ
