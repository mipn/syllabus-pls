~"semestre 4"

# Mineure parcours P2S : Monitoring de systèmes cyber-physiques
Ce module s'intéresse au monitoring de systèmes cyber-physiques vis-à-vis de propriétés de sûreté et de sécurité à l'aide de méthodes formelles.

Mots-clés : systèmes temps-réel, cybersécurité, méthodes formelles, monitoring, automates

Enseignant : Étienne André
