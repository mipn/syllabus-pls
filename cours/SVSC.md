~"spécifications-sûreté-sécurité" ~"semestre 3"

# Spécification et vérification de systèmes critiques

Intervenant 2023-2024 : Étienne André

## Prérequis
* théorie des graphes et automates
* programmation

## Objectifs
* compréhension de la nécessité des spécification et vérification formelles
* initiation aux techniques de méthodes formelles automatisées

## Compétences visées
* spécifier formellement un système concurrent
* spécifier formellement des propriétés de correction dans des logiques temporelles
* comprendre et appliquer des algorithmes de model checking

## Contenu du cours
- automates et spécifications formelles
- logiques temporelles
- algorithmes de model checking
- propriétés d'accessibilité
- model checking symbolique

## Éléments d'évaluation
- (Pa + Pr) / 2

## Bibliographie
* Bérard et al.: "Systems and Software Verification, Model-Checking Techniques and Tools", Springer
* Baier and Katoen: "Principles of Model Checking", MIT Press
