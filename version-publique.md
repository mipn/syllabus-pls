# Syllabus P2S

Description des enseignements du parcours « programmation, sûreté et sécurité ».

Chaque étudiant ou étudiante suit les cours suivants :

1. Programmation
    - [Développement JavaScript avancé et ubiquitaire](cours/JSAU.md) (3 ECTS)
    - [Environnements de développements libres](cours/EDL.md) (3 ECTS)
    - [Méthodologies et Langages Avancés](cours/MLA.md) (3 ECTS)
    - [Programmation modulaire et résiliente](cours/PMR.md) (3 ECTS)
2. Spécification, sûreté et sécurité
    - [Programmes et preuves](cours/PP.md) (3 ECTS)
    - [Réseaux de Petri de haut niveau](cours/RPHN.md) (3 ECTS)
    - [Sûreté et sécurité](cours/SurSec.md) (3 ECTS)
    - [Spécification et vérification de systèmes critiques](cours/SVSC.md) (3 ECTS)
3. Enseignements transversaux
    - Anglais (2 ECTS)
    - Innovation (2 ECTS)
    - Soft skills (2 ECTS)
    - Libre (sport, mobilité internationale, activité associative) (Bonus)
4. Deux options parmi :
    - [Cartographie et OpenStreetMap](cours/option-OSM.md) (3 ECTS)
    - Détection d'intrusions (3 ECTS)
    - [Maîtrise des crises dans les environnements critiques](cours/option-LiveM.md) (3 ECTS)
    - [Monitoring de systèmes cyber-physiques](cours/option-MSCP.md) (3 ECTS)
    - [Analyse des réseaux d'interactions](cours/option-ARS.md) (3 ECTS)
    - [Apprentissage et métaapprentissage appliqués à l'IoT](cours/option-ML-IoT.md) (3 ECTS)
    - Cybersécurité et IA (3 ECTS)
    - [Design et eXperience utilisateur](cours/option-DTUX.md) (3 ECTS)
    - Informatique quantique (3 ECTS)


5. un stage de 4 à 6 mois dans un laboratoire de recherche ou en entreprise.
